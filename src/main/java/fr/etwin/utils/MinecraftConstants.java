package fr.etwin.utils;

public class MinecraftConstants {
    public static final int CHUNK_WIDTH = 16;
    public static final int CHUNK_MIN_HEIGHT = -64;
    public static final int CHUNK_MAX_HEIGHT = 320;
    public static final int CHUNK_HEIGHT = -CHUNK_MIN_HEIGHT + CHUNK_MAX_HEIGHT;
    public static final int CHUNK_SIZE = CHUNK_WIDTH * CHUNK_WIDTH * CHUNK_HEIGHT;

    public static final int CHUNK_PER_ZONE = 2;
    public static final int ZONE_WIDTH = CHUNK_PER_ZONE * CHUNK_WIDTH;
}
